import 'dart:collection';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Color color = Theme.of(context).primaryColor;

    Widget imageSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'images/dark.jpg',
            width: 200,
            height: 200,
            fit: BoxFit.cover,
          ),
          Image.asset(
            'images/hide.jpg',
            width: 200,
            height: 200,
            fit: BoxFit.cover,
          ),
          Image.asset(
            'images/take.jpg',
            width: 200,
            height: 200,
            fit: BoxFit.cover,
          ),
        ],
      ),
    );
    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'Name   : Khajonpong Gungumthonwong \n\n'
        'E-mail : Khajonpong.g@gmail.com \n\n'
        'Phone  : 08005052__ \n\n'
        'Hobby  : Nap, Take Photo, Cook, Feed pet, Stay alive ! \n\n'
        'Dream  : Know what i like \n\n',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontStyle: FontStyle.italic,
        ),
        softWrap: true,
      ),
    );
    Widget iconSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'images/boot.png',
            width: 100,
            height: 100,
            fit: BoxFit.fill,
          ),
          Image.asset(
            'images/html.png',
            width: 300,
            height: 100,
            fit: BoxFit.fill,
          ),
          Image.asset(
            'images/java.jpg',
            width: 100,
            height: 100,
            fit: BoxFit.cover,
          ),
          Image.asset(
            'images/swift.png',
            width: 100,
            height: 100,
            fit: BoxFit.fill,
          ),
        ],
      ),
    );
    Widget stuSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            children: [
              Container(
                child: Text('High School'),
                color: Colors.green,
                margin: EdgeInsets.only(left: 20, bottom: 40, top: 50),
                padding: EdgeInsets.all(40),
              ),
              Image.asset(
                'images/rb.jpg',
                width: 200,
                height: 200,
                fit: BoxFit.cover,
              ),
            ],
          ),
          Column(
            children: [
              Container(
                child: Text('University'),
                color: Colors.brown,
                margin: EdgeInsets.only(left: 20, bottom: 40, top: 50),
                padding: EdgeInsets.all(40),
              ),
              Image.asset(
                'images/Buu.png',
                width: 200,
                height: 200,
                fit: BoxFit.cover,
              ),
            ],
          )
        ],
      ),
    );

    return MaterialApp(
      title: 'My Resume',
      home: Scaffold(
        body: ListView(
          children: [
            imageSection,
            textSection,
            iconSection,
            stuSection,
          ],
        ),
      ),
    );
  }
}
